// Import de la bibliothèque JQuery
var jquery = require("jquery");
window.$ = window.jQuery = jquery; // définition des variables globales
require("jquery-ui-dist/jquery-ui.js");

// Récupération du fichier json contenant les données sur les montagnes et leurs stations (nom, BRA...)
$(function() {
    $.getJSON("ressources/data/mountainsWGS84.json", createHTML);
});

// Fonction qui ouvre une nouvelle fenêtre contenant le pdf de MétéoFrance sur le BRA de la station sélectionnée
window.getMeteo = function getMeteo(elt){
	var idmf = elt.value;
	window.open("http://www.meteofrance.com/integration/sim-portail/generated/integration/img/produits/pdf/bulletins_bra/"+idmf+".pdf");
}

// Fonction raccourci qui récupère l'identifiant de l'objet sélectionné
window._ = function _(elmt) {
	return document.getElementById(elmt);
}

// Fonction qui affiche une div lorsque l'on sélectionne le bouton qui lui correspond (passage du display "none" au display "block")
window.visibilite = function visibilite(thingId) {
	var ids = ["MRD", "MRP", "MRE"];
	if (ids.includes(thingId)) {
		thingId = "div_"+thingId;
	}
    var targetElement = _(thingId);
    var elements = document.getElementsByClassName("Element")
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.display = "none" ;
    }
    if (targetElement.style.display == "none") {
            targetElement.style.display = "" ;
    }
}

// Création des menus déroulants pour sélectionner sa station de montagne par récupération des informations dans "mountainsWGS84.json"
window.createHTML = function createHTML(objet) {
	var liste_montagnes = [];
  console.log(liste_montagnes);
	for (const feature in objet["features"]) {
		var domaine = objet["features"][feature];
		if (liste_montagnes.indexOf(domaine["properties"]["mountain"]) === -1) {
			liste_montagnes.push(domaine["properties"]["mountain"]);
		}
	}

	for (var i = 0; i < liste_montagnes.length; i++) {
		var newOption = document.createElement('option');
		var newContent = document.createTextNode(liste_montagnes[i]);
  		newOption.appendChild(newContent);

		_('bulletinBRA').appendChild(newOption);
	}

	for (var i = 0; i < liste_montagnes.length; i++) {
		chaine_mont = liste_montagnes[i];
		var div_ss_chaine = document.createElement('div');
		div_ss_chaine.className = "Element";
		div_ss_chaine.id = chaine_mont;
		div_ss_chaine.style.display = "none";

		var newFieldset = document.createElement('fieldset');
		div_ss_chaine.appendChild(newFieldset);

		var label = document.createElement('label');
		var txt_label = document.createTextNode(chaine_mont);
		label.appendChild(txt_label);
		newFieldset.appendChild(label);

		var newSelect = document.createElement('select');
		newSelect.setAttribute("onchange", "getMeteo(this)");
		newSelect.id = "bulletin "+chaine_mont;
		newOptionFirst = document.createElement('option');
		newOptionFirst.setAttribute("disabled", "disabled");
		newOptionFirst.setAttribute("selected", "selected");
		newOptionFirst.appendChild(document.createTextNode("Sélectionner :"));
		newSelect.appendChild(newOptionFirst);


		var liste_domaine = [];
		for (const feature in objet["features"]) {
			var domaine = objet["features"][feature];
			if (liste_domaine.indexOf(domaine["properties"]["title"]) === -1) {
				intermed_liste = [domaine["properties"]["title"], domaine["properties"]["id_mf"], domaine["properties"]["mountain"]];
				liste_domaine.push(intermed_liste);
			}
		}

		for (var j = 0; j < liste_domaine.length; j++) {
			if (liste_domaine[j][2] == chaine_mont) {
				newOptionSD = document.createElement('option');
				newOptionSD.value = liste_domaine[j][1];
				if (liste_domaine[j][1] == null) {
					newOptionSD.setAttribute("disabled", "disabled");
				}
				text_option = document.createTextNode(liste_domaine[j][0]);
				newOptionSD.appendChild(text_option);
				newSelect.appendChild(newOptionSD);
			}
		}

		newFieldset.appendChild(newSelect);

		_("requete").insertBefore(div_ss_chaine, _("titre2"));
	}
}

// Récupération de la variable id_mf pour créer le lien HTML menant aux pdf de MétéoFrance
window.FunctionBRA = function FunctionBRA() {
	var montagne_select = _("bulletinBRA").value;
	visibilite(montagne_select);
}

window.FunctionMeteo = function FunctionMeteo(button){
	console.log('Voici les données pour l envoi de la requête au serveur Yéti '+button.name+' / '+(_("bulletin "+button.name).value));
}

// js pour les seuils d'altitudes
window.checkAlti = function checkAlti(){
	var risque_bas = document.getElementById("risque_bas");
	var seuil_alti = document.getElementById("seuil_alti");
	var bra_checked = document.getElementById("scales");

	var seuil = document.createElement("input");
	seuil.type = "text";
	seuil.name = "risque_bas";

	var menuSelect = document.createElement("select");
	for(let i = 1; i < 5; i++){
		var option = document.createElement('option');
		option.text = i;
		menuSelect.add(option);
	}

	if((bra_checked.checked==true)&&(seuil_alti.hasChildNodes()==false)){
		seuil_alti.appendChild(seuil);
		risque_bas.appendChild(menuSelect);
	}
}
