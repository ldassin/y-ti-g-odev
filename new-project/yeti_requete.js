// ----- Importations -----

import 'ol/ol.css';
import Feature from 'ol/Feature';
import Map from 'ol/Map';
import View from 'ol/View';
import GeoJSON from 'ol/format/GeoJSON';
import Circle from 'ol/geom/Circle';
import {Tile as TileLayer, Image as ImageLayer, Vector as VectorLayer} from 'ol/layer';
import {OSM,Tile as TileSource,ImageStatic as ImageSource , Vector as VectorSource} from 'ol/source';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
import Projection from 'ol/proj/Projection';
import Static from 'ol/source/ImageStatic';


// ----- Definition des styles pour les couches vectorielles -----

var image_point = new CircleStyle({
  radius: 5,
  fill: null,
  stroke: new Stroke({color: 'red', width: 1})
});

var styles = {
  'Point': new Style({
    image: image_point
  }),
  'MultiLineString': new Style({
    stroke: new Stroke({
      color: 'red',
      width: 1
    })
  }),
  'Polygon': new Style({
    stroke: new Stroke({
      color: 'yellow',
      lineDash: [4],
      width: 3
    }),
    fill: new Fill({
      color: 'rgba(255, 255, 0, 0.05)'
    })
  })
};

var styleFunction = function(feature) {
  return styles[feature.getGeometry().getType()];
};


// ----- Création de l'objet Carte -----

var map = new Map({
  layers: [
    new TileLayer({
      source: new OSM()
    })
  ],
  target: 'map',
  view: new View({
    projection: 'EPSG:4326',
    center: [1.75, 46.75],
    zoom: 6
  })
});

// ----- Partie Requete -----

// Initialisation de variables par defauts
var bcalculrisque = document.getElementById("Bcalculrisque");
var methode = "mrd";
var bbox = "5.903434753417969,44.96431209836382,5.97209930419922,45.00073807829067";
var risque_haut = "0";
var risque_bas = risque_haut;
var seuil_alti = "0";
var rdv = ["none"];
var potentiel_danger = "0";
var neige_mouillee = "false";
var cdata = [];
var taille_groupe = "0";

// -- Ajout d'un listener sur la carte pour recuperer la bbox et le zoom --
function onMoveEnd(evt) {
  var map = evt.map;
  var extent = map.getView().calculateExtent(map.getSize());
  var left = extent[0];
  var bottom = extent[1];
  var right = extent[2];
  var top = extent[3];
  bbox = left.toString(10) + "," + bottom.toString(10) + "," + right.toString(10) + "," + top.toString(10);
  zoom = map.getView().getZoom();

  // affichage du zoom, a modifier pour coller a la mise en page :
  document.getElementById("zoom").innerHTML = ("zoom : " + zoom).substring(0,11);
}
map.on('moveend', onMoveEnd);


// Récupération des éléments formant les branches de la Rose des Vents
let paths = document.querySelectorAll('path');
paths.forEach(path => {
    // A chaque <path> on attribue la classe 'g'
  path.addEventListener('click', e => {
    path.parentNode.classList.add('g');
    // On vérifie que le tableau rdv ne contient pas déjà l'id de la balise <path>
    if(rdv.includes(path.id) == false){
      rdv.push(path.id);
    }else{  // si c'est le cas c'est que l'utilisateur veut enlever la direction choisie
      var pos = rdv.indexOf(path.id);
      rdv.splice(pos, 1);
      path.parentNode.classList.remove('g');
    }
  })
})

// Variable contenant toutes puces <li>
let items = document.querySelectorAll('#frise_potentiel li');
let pot_bra = document.getElementById('potentiel_bra');

// Récupération de la valeur du BRA sélectionnée
let range = document.getElementById('braSelect');
range.addEventListener('input', function(e){
  let value = range.value;
  potentiel_danger = value;
  
  // Selon la valeur du BRA, on attribue la classe opacity à une plage de valeurs correspondantes
  if(value=="1"){
    pot_bra.innerText="Potentiel de danger: "+items[1].innerText+" (BRA:1)";
    items.forEach(function(item, i) {
      if(i<2){
        if(i==1){
          item.classList.add('border'); // La classe border ne concerne que certaines valeurs "par défaut"
        }
        item.classList.add('opacity');
      }else if(i>=2){
        if(i!=1){
          item.classList.remove('border');
        }
        item.classList.remove('opacity');
      }
    });
  }
  if(value=="2"){
    pot_bra.innerText="Potentiel de danger: "+items[3].innerText+" (BRA:2)";
    items.forEach(function(item, i) {
      if((2<=i) && (i<=5)){
        if(i==3){
          item.classList.add('border');
        }
        item.classList.add('opacity');
      }else{
        if(i!=3){
          item.classList.remove('border');
        }
        item.classList.remove('opacity');
      }
    });
  }
  if(value=="3"){
    pot_bra.innerText="Potentiel de danger:   "+items[7].innerText+" (BRA:3)";
    items.forEach(function(item, i) {
      if((5<=i) && (i<=11)){
        if(i==7){
          item.classList.add('border');
        }
        item.classList.add('opacity');
      }else{
        if(i!=7){
          item.classList.remove('border');
        }
        item.classList.remove('opacity');
      }
    });
  }
  if(value=="4"){
    pot_bra.innerText="Potentiel de danger: "+items[15].innerText+" (BRA:4)";
    items.forEach(function(item, i) {
      if(12<=i){
        if(i==15){
          item.classList.add('border');
        }
        item.classList.add('opacity');
      }else if(i<12){
        if(i!=15){
          item.classList.remove('border');
        }
        item.classList.remove('opacity');
      }
    });
  }

})

// On ajoue à chacune des puces un écouteur d'évènement "click"
for(var i = 0; i < items.length; i++){
  items[i].addEventListener("click", function(e){
    items.forEach((item, i) => {
      if(item.innerText==e.target.innerText){   // On regarde si l'élément cliqué 
      // ne possède pas déjà la classe 'border' et on lui attribue le cas échéant
        item.classList.add('border');
        potentiel_danger = i+1;
      }else{
        item.classList.remove('border');
      }
    });
  })
}


// -- Ajout d'un listener sur le bouton de validation pour lancer la requete et afficher le résultat --
window.valider = function valider () {

  // Limitation sur le zoom
  if (zoom < 13) {
    alert("zoom insufisant")
  } else {

    // Recuperation des paramètres du formulaire lors du "click" sur le bouton "Calculer le risque"
    var choix_risque = document.getElementById('braSelect');
    risque_haut = choix_risque.value;
    if(risque_haut=="0"){   // Dans ce cas-ci, aucune valeur de BRA n'a été sélectionné
      // On affiche alors un message pop-up à l'utilisateur
      alert("La valeur de BRA est manquante. Veuillez saisir la valeur spécifiée par le bulletin Météo-France.");
    }else{  // Sinon, on regarde quelle méthode a été choisie
      var choix_methode = document.getElementsByName('methode');
      for(var i = 0; i < choix_methode.length; i++){
       if(choix_methode[i].checked){
         methode = choix_methode[i].value;
       }
      }

      var diff_bra = document.getElementById('scales');
      if(diff_bra.checked==true){   // Si la case "BRA haut/bas différents ?" est coché
        var choix_risque_bas = document.getElementById('risque_bas');
        risque_bas = choix_risque_bas.children[0].value;    // on ajoute un menu de sélection
        var choix_seuil = document.getElementById('seuil_alti');
        seuil_alti = choix_seuil.children[0].value; // on ajoute un champ texte 
        // pour connaître le seuil d'altitude à partir duquel change l'indice de BRA
      }else{
        risque_bas = risque_haut;
      }
      
      // Si la case "Neige mouillé" est coché, on l'ajoute dans la requête
      neige_mouillee = document.getElementById('neige_mouillee').checked;
      var choix_groupe = document.getElementsByName('groupe');
      for(var i = 0; i < choix_groupe.length; i++){
       if(choix_groupe[i].checked){
         taille_groupe = choix_groupe[i].value;
       }
      }
      
      // Pour éviter qu'une requête "débutante" effectué après une requête "experte" ou "élémentaire",
      // on change la forme de la requête en fonction de la méthode choisie
      if(methode=="mrd"){
        var req = "methode=" + methode + ";bbox="  + bbox + ";risque_haut=" + risque_haut + ";risque_bas=" + risque_bas + ";seuil_alti=" + seuil_alti;
      }else if(methode=="mre"){
        var req = "methode=" + methode + ";bbox="  + bbox + ";risque_haut=" + risque_haut + ";risque_bas=" + risque_bas + ";seuil_alti=" + seuil_alti + ";rdv=" + rdv;
      }else if(methode="mrp"){
        var req = "methode=" + methode + ";bbox="  + bbox + ";risque_haut=" + risque_haut + ";risque_bas=" + risque_bas + ";seuil_alti=" + seuil_alti + ";potentiel_danger=" + potentiel_danger + ";neige_mouillee=" + neige_mouillee + ";taille_groupe=" + taille_groupe;
      }

      // Envoie de la requete
      cdata=[]; // Réinitialisation du tableau contenant les chaînes de caractère du fichier XML
      // que le serveur nous envoie
      fetch('https://api.ensg.eu/yeti-wps?request=Execute&service=WPS&version=1.0.0&identifier=Yeti&datainputs=' + req, {
        method: 'get'
      })
      .then(r => r.text())
      .then(r => {
      var parser = new DOMParser();
      var xml = parser.parseFromString(r,"text/xml");
              recuprecursif(xml);   
              var img = new Image();
              var b64 = cdata[0];
              img.src = "data:image/png;base64," + b64;  // recuperation de l'image
              bbox = cdata[1].split(',');
              var left = bbox[0];                        // recuperaion de l'emprise
              var bottom = bbox[1];
              var right = bbox[2];
              var top = bbox[3];
              var epsg = bbox[4];                        // recuperation de la projection

              // -- Ajout De la couche emprise --
              // Creation d'un objet geojson affichant l'emprise dans une couche vecteur
              var geojsonObject = {
                'type': 'FeatureCollection',
                'crs': {
                  'type': 'name',
                  'properties': {
                    'name': epsg
                  }
                },
                'features': [{
                  'type': 'Feature',
                  'geometry': {
                    'type': 'Polygon',
                    'coordinates': [[[left, bottom], [right, bottom], [right, top], [left,top]]]
                  }
                }]
              };

              var vectorSource = new VectorSource({
                features: (new GeoJSON()).readFeatures(geojsonObject)
              });

              var vectorLayer = new VectorLayer({
                source: vectorSource,
                style: styleFunction
              });

              map.addLayer(vectorLayer);


              // -- Ajout De la couche "zone à risque" --
              // Affichage de l'image résultante de la requête dans un couche raster
              // 3 parametres sont utilises : l'image, l'emprise, la projection.
              map.addLayer(new ImageLayer({
                source: new ImageSource({
                  url: img.getAttribute("src"),
                  imageExtent: [left, bottom, right, top],
                  projection: epsg
                  })
                  ,opacity: 0.45                  // On rend la couche transparente
                })
              );

            })
    }
  }
}

/* ajoute l’événement */
bcalculrisque.addEventListener('onclick', valider);

/**/
function recuprecursif(xml){
  var result="";
  var racine = xml.documentElement;
  getFils(racine);
}
/**/
function getFils(xml){
  for(var i = 0; i<xml.childNodes.length; i++){
    var element = xml.childNodes[i];

    if(element=="[object CDATASection]"){
      cdata.push(element.nodeValue);
    }
    if(element.nodeType==1){
          getFils(element);
      }
  }
}

function checkAlti(){   // Fonction intervenant lorsque l'on coche
                        // la case "BRA haut/bas différents ?"
                        
    // On récupère les <div> 
	var risque = document.getElementById("risque_bas");
	var alti = document.getElementById("seuil_alti");
	var bra_checked = document.getElementById("scales");
    
    // Création d'un élément <input>
	var seuil = document.createElement("input");
	seuil.type = "text";
	seuil.name = "risque_bas";
    seuil.id = "seuil";

    // Création d'un menu de sélection
	var menuSelect = document.createElement("select");
  menuSelect.id = "menuSelect";
  var option = document.createElement('option');
  option.text = " ";
  menuSelect.add(option);
	for(let i = 1; i < 5; i++){
		var option = document.createElement('option');
		option.text = i;
		menuSelect.add(option);
	}

	if((bra_checked.checked==true)&&(alti.hasChildNodes()==false)){
		alti.appendChild(seuil);
		risque.appendChild(menuSelect);
	}else{
    alti.removeChild(alti.children[0]);
    risque.removeChild(risque.children[0]);
  }
}
