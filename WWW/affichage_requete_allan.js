// ----- Importations -----

import 'ol/ol.css';
import Feature from 'ol/Feature';
import Map from 'ol/Map';
import View from 'ol/View';
import GeoJSON from 'ol/format/GeoJSON';
import Circle from 'ol/geom/Circle';
import {Tile as TileLayer, Image as ImageLayer, Vector as VectorLayer} from 'ol/layer';
import {OSM,Tile as TileSource,ImageStatic as ImageSource , Vector as VectorSource} from 'ol/source';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
import Projection from 'ol/proj/Projection';
import Static from 'ol/source/ImageStatic';


// ----- Definition des styles -----

var image_point = new CircleStyle({
  radius: 5,
  fill: null,
  stroke: new Stroke({color: 'red', width: 1})
});

var styles = {
  'Point': new Style({
    image: image_point
  }),
  'MultiLineString': new Style({
    stroke: new Stroke({
      color: 'red',
      width: 1
    })
  }),
  'Polygon': new Style({
    stroke: new Stroke({
      color: 'yellow',
      lineDash: [4],
      width: 3
    }),
    fill: new Fill({
      color: 'rgba(255, 255, 0, 0.05)'
    })
  })
};

var styleFunction = function(feature) {
  return styles[feature.getGeometry().getType()];
};


// ----- Création de l'objet Carte -----

var map = new Map({
  layers: [
    new TileLayer({
      source: new OSM()
    })
  ],
  target: 'map',
  view: new View({
    projection: 'EPSG:4326',
    center: [1.75, 46.75],
    zoom: 6
  })
});

// ----- Partie Requete -----

// -- Initialisation des variables --
var form_yeti = document.getElementById("requete");
var methode = "mrd";
var bbox = "5.903434753417969,44.96431209836382,5.97209930419922,45.00073807829067";
var risque_haut = "1";
var risque_bas = risque_haut;
var seuil_alti = "0";
var rdv = "none";
var potentiel_danger = "0";
var neige_mouillee = "false";
var taille_groupe = "0";
var cdata = [];
var zoom = 6

// -- Ajout d'un listener sur la carte pour recuperer la bbox et le zoom --
function onMoveEnd(evt) {
  var map = evt.map;
  var extent = map.getView().calculateExtent(map.getSize());
  var left = extent[0];
  var bottom = extent[1];
  var right = extent[2];
  var top = extent[3];
  bbox = left.toString(10) + "," + bottom.toString(10) + "," + right.toString(10) + "," + top.toString(10);
  zoom = map.getView().getZoom();

  // affichage du zoom, achanger selon la mise en page :
  document.getElementById("zoom").innerHTML = ("zoom : " + zoom).substring(0,11);
}
map.on('moveend', onMoveEnd);

// -- Listener pour effectuer le fetch sur le serveur de requete et
//    créer une nouvelle couche contenant le résultat --
function valider (event) {
    // -- Partie ou l'on récupère les valeurs du formulaire --

    //methode = form_yeti.elements["methode"];
    //risque_haut = form_yeti.elements["bra"];
    //risque_bas = risque_haut;
    //neige_mouillee = form_yeti.elements["neige_mouillee"];
    //taille_groupe = form_yeti.elements["groupe"];

    // -- On ne lance pas la requête si le zoom est inférieur à 13 

    if (zoom < 13) {
      alert("zoom insufisant")
    } else {

      cdata = [];

      var req = "methode=" + methode + ";bbox="  + bbox + ";risque_haut=" + risque_haut + ";risque_bas=" + risque_bas + ";seuil_alti=" + seuil_alti + ";rdv=" + rdv + ";potentiel_danger=" + potentiel_danger + ";neige_mouillee=" + neige_mouillee + ";taille_groupe=" + taille_groupe;

      fetch('https://api.ensg.eu/yeti-wps?request=Execute&service=WPS&version=1.0.0&identifier=Yeti&datainputs=' + req, {
        method: 'get'
      })
      .then(r => r.text())
      .then(r => {
      var parser = new DOMParser();
      var xml = parser.parseFromString(r,"text/xml");
              console.log(recuprecursif(xml));
              var img = new Image();
              var b64 = cdata[0];
              img.src = "data:image/png;base64," + b64;
              bbox = cdata[1].split(',');
              var left = bbox[0];
              var bottom = bbox[1];
              var right = bbox[2];
              var top = bbox[3];
              var epsg = bbox[4];

              // -- Ajout De la couche emprise --
              var geojsonObject = {
                'type': 'FeatureCollection',
                'crs': {
                  'type': 'name',
                  'properties': {
                    'name': epsg
                  }
                },
                'features': [{
                  'type': 'Feature',
                  'geometry': {
                    'type': 'Polygon',
                    'coordinates': [[[left, bottom], [right, bottom], [right, top], [left,top]]]
                  }
                }]
              };

              var vectorSource = new VectorSource({
                features: (new GeoJSON()).readFeatures(geojsonObject)
              });

              var vectorLayer = new VectorLayer({
                source: vectorSource,
                style: styleFunction
              });

              map.addLayer(vectorLayer);


              // -- Ajout De la couche "zone à risque" --

              map.addLayer(new ImageLayer({
                source: new ImageSource({
                  url: img.getAttribute("src"),
                  imageExtent: [left, bottom, right, top],
                  projection: epsg
                  })
                  ,opacity: 0.35
                })
              );

            })
    }
}
form_yeti.addEventListener('submit', valider);

/**/
function recuprecursif(xml){
  var result="";
  var racine = xml.documentElement;
  getFils(racine);
}
/**/
function getFils(xml){
  // document.write( "<b>"+xml.nodeName +"</b> | Type "+ xml.nodeType+" | "+ xml.childNodes.length+ " fils");
  for(var i = 0; i<xml.childNodes.length; i++){
    var element = xml.childNodes[i];

    if(element=="[object CDATASection]"){
      cdata.push(element.nodeValue);
    }
    if(element.nodeType==1){
          getFils(element);
      }
      // else if(element.nodeType==3 || element.nodeType==4){
      //     document.write(" valeur = "+ element.nodeValue);
      // }
  }
}
