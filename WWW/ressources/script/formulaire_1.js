$(document).ready(function() {
	$.get("ressources/data/mountainsWGS84.json", createHTML);
});

function _(elmt) {
	return document.getElementById(elmt);
}

function visibilite(thingId){
	var ids = ["MRD", "MRP", "MRE"];
	if (ids.includes(thingId)) {
		thingId = "div_"+thingId;
	}
    var targetElement = _(thingId);
    var elements = document.getElementsByClassName("Element")
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.display = "none" ;
    }
    if (targetElement.style.display == "none") {
            targetElement.style.display = "" ;
    }
}

function createHTML(objet) {
	var liste_montagnes = [];
	for (const feature in objet["features"]) {
		var domaine = objet["features"][feature];
		if (liste_montagnes.indexOf(domaine["properties"]["mountain"]) === -1) {
			liste_montagnes.push(domaine["properties"]["mountain"]);
		}
	}

	for (var i = 0; i < liste_montagnes.length; i++) {
		var newOption = document.createElement('option');
		var newContent = document.createTextNode(liste_montagnes[i]);
  		newOption.appendChild(newContent);

		_('bulletinBRA').appendChild(newOption);
	}

	for (var i = 0; i < liste_montagnes.length; i++) {
		chaine_mont = liste_montagnes[i];
		var div_ss_chaine = document.createElement('div');
		div_ss_chaine.className = "Element";
		div_ss_chaine.id = chaine_mont;
		div_ss_chaine.style.display = "none";

		var newFieldset = document.createElement('fieldset');
		div_ss_chaine.appendChild(newFieldset);

		var label = document.createElement('label');
		var txt_label = document.createTextNode(chaine_mont);
		label.appendChild(txt_label);
		newFieldset.appendChild(label);

		var newSelect = document.createElement('select');
		newSelect.id = "bulletin "+chaine_mont;

		var liste_domaine = [];
		for (const feature in objet["features"]) {
			var domaine = objet["features"][feature];
			if (liste_domaine.indexOf(domaine["properties"]["title"]) === -1) {
				intermed_liste = [domaine["properties"]["title"], domaine["properties"]["id_mf"], domaine["properties"]["mountain"]];
				liste_domaine.push(intermed_liste);
			}
		}

		for (var j = 0; j < liste_domaine.length; j++) {
			if (liste_domaine[j][2] == chaine_mont) {
				newOptionSD = document.createElement('option');
				newOptionSD.value = liste_domaine[j][0];
				if (liste_domaine[j][1] == null) {
					newOptionSD.setAttribute("disabled", "disabled");
				}
				text_option = document.createTextNode(liste_domaine[j][0]);
				newOptionSD.appendChild(text_option);
				newSelect.appendChild(newOptionSD);
			}
		}

		newFieldset.appendChild(newSelect);
		newInput = document.createElement('input');
		newInput.type = "button";
		newInput.name = chaine_mont;
		newInput.value = "Valider";
		newInput.setAttribute("onclick", "FunctionMeteo(this)");
		newFieldset.appendChild(newInput);

		document.body.insertBefore(div_ss_chaine, _("Methodes"));
	}
}

function FunctionBRA() {
	var montagne_select = _("bulletinBRA").value;
	visibilite(montagne_select);
}

function FunctionMeteo(button){
	console.log('à ton tour Lucas ! '+button.name+' / '+(_("bulletin "+button.name).value));
}

function checkAlti(){
	var risque_bas = document.getElementById("risque_bas");
	var seuil_alti = document.getElementById("seuil_alti");
	var bra_checked = document.getElementById("scales");

	var seuil = document.createElement("input");
	seuil.type = "text";
	seuil.name = "risque_bas";

	var menuSelect = document.createElement("select");
	for(let i = 1; i < 5; i++){
		var option = document.createElement('option');
		option.text = i;
		menuSelect.add(option);
	}

	if((bra_checked.checked==true)&&(seuil_alti.hasChildNodes()==false)){
		seuil_alti.appendChild(seuil);
		risque_bas.appendChild(menuSelect);
	}
}
