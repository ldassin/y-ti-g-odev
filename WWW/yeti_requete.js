var bcalculrisque = document.getElementById("Bcalculrisque");
var methode = "mrd";
var bbox = "5.903434753417969,44.96431209836382,5.97209930419922,45.00073807829067";
var risque_haut = "0";
var risque_bas = risque_haut;
var seuil_alti = "0";
var rdv = ["none"];
var potentiel_danger = "0";
var neige_mouillee = "false";
var cdata = [];
// const lat = (45.00073807829067-44.96431209836382)/2;
// const lng = (5.97209930419922-5.903434753417969)/2;
var taille_groupe = "0";

let paths = document.querySelectorAll('path');
paths.forEach(path => {
  path.addEventListener('click', e => {
    path.parentNode.classList.add('g');
    if(rdv.includes(path.id) == false){
      rdv.push(path.id);
    }else{
      var pos = rdv.indexOf(path.id);
      rdv.splice(pos, 1);
      path.parentNode.classList.remove('g');
    }
  })
})

let items = document.querySelectorAll('#frise_potentiel li');
let pot_bra = document.getElementById('potentiel_bra');

let range = document.getElementById('braSelect');
range.addEventListener('input', function(e){
  // recup valeur
  let value = range.value;
  potentiel_danger = value;

  if(value=="1"){
    pot_bra.innerText="Potentiel de danger: "+items[1].innerText+" (BRA:1)";
    items.forEach(function(item, i) {
      if(i<2){
        if(i==1){
          item.classList.add('border');
        }
        item.classList.add('opacity');
      }else if(i>=2){
        if(i!=1){
          item.classList.remove('border');
        }
        item.classList.remove('opacity');
      }
    });
  }
  if(value=="2"){
    pot_bra.innerText="Potentiel de danger: "+items[3].innerText+" (BRA:2)";
    items.forEach(function(item, i) {
      if((2<=i) && (i<=5)){
        if(i==3){
          item.classList.add('border');
        }
        item.classList.add('opacity');
      }else{
        if(i!=3){
          item.classList.remove('border');
        }
        item.classList.remove('opacity');
      }
    });
  }
  if(value=="3"){
    pot_bra.innerText="Potentiel de danger:   "+items[7].innerText+" (BRA:3)";
    items.forEach(function(item, i) {
      if((5<=i) && (i<=11)){
        if(i==7){
          item.classList.add('border');
        }
        item.classList.add('opacity');
      }else{
        if(i!=7){
          item.classList.remove('border');
        }
        item.classList.remove('opacity');
      }
    });
  }
  if(value=="4"){
    pot_bra.innerText="Potentiel de danger: "+items[15].innerText+" (BRA:4)";
    items.forEach(function(item, i) {
      if(12<=i){
        if(i==15){
          item.classList.add('border');
        }
        item.classList.add('opacity');
      }else if(i<12){
        if(i!=15){
          item.classList.remove('border');
        }
        item.classList.remove('opacity');
      }
    });
  }

})

for(var i = 0; i < items.length; i++){
  items[i].addEventListener("click", function(e){
    items.forEach((item, i) => {
      if(item.innerText==e.target.innerText){
        item.classList.add('border');
        potentiel_danger = i+1;
      }else{
        item.classList.remove('border');
      }
    });
  })
}

function valider () {

    var choix_risque = document.getElementById('braSelect');
    risque_haut = choix_risque.value;
    console.log(risque_haut);
    if(risque_haut=="0"){
      alert("La valeur de BRA est manquante. Veuillez saisir la valeur spécifiée par le bulletin Météo-France.");
    }else{
      var choix_methode = document.getElementsByName('methode');
      for(var i = 0; i < choix_methode.length; i++){
       if(choix_methode[i].checked){
         methode = choix_methode[i].value;
       }
      }

      var diff_bra = document.getElementById('scales');
      if(diff_bra.checked==true){
        var choix_risque_bas = document.getElementById('risque_bas');
        risque_bas = choix_risque_bas.children[0].value;
        var choix_seuil = document.getElementById('seuil_alti');
        seuil_alti = choix_seuil.children[0].value;
      }else{
        risque_bas = risque_haut;
      }

      neige_mouillee = document.getElementById('neige_mouillee').checked;
      var choix_groupe = document.getElementsByName('groupe');
      for(var i = 0; i < choix_groupe.length; i++){
       if(choix_groupe[i].checked){
         taille_groupe = choix_groupe[i].value;
       }
      }

      if(methode=="mrd"){
        var req = "methode=" + methode + ";bbox="  + bbox + ";risque_haut=" + risque_haut + ";risque_bas=" + risque_bas + ";seuil_alti=" + seuil_alti;
      }else if(methode=="mre"){
        var req = "methode=" + methode + ";bbox="  + bbox + ";risque_haut=" + risque_haut + ";risque_bas=" + risque_bas + ";seuil_alti=" + seuil_alti + ";rdv=" + rdv;
      }else if(methode="mrp"){
        var req = "methode=" + methode + ";bbox="  + bbox + ";risque_haut=" + risque_haut + ";risque_bas=" + risque_bas + ";seuil_alti=" + seuil_alti + ";potentiel_danger=" + potentiel_danger + ";neige_mouillee=" + neige_mouillee + ";taille_groupe=" + taille_groupe;
      }
      console.log(req);
      cdata=[];
      fetch('https://api.ensg.eu/yeti-wps?request=Execute&service=WPS&version=1.0.0&identifier=Yeti&datainputs=' + req, {
        method: 'get'
      })
      .then(r => r.text())
      .then(r => {
      parser = new DOMParser();
      xml = parser.parseFromString(r,"text/xml");
              recuprecursif(xml);
              var img = new Image();
              var b64 = cdata[0];
              img.src = "data:image/png;base64," + b64;
              bbox = cdata[1].split(',');
              var left = bbox[0];
              var bottom = bbox[1];
              var right = bbox[2];
              var top = bbox[3];
              var epsg = bbox[4].split(":")[1];
              document.getElementById("requete").appendChild(img);
      })
    }
}

/* ajoute l’événement */
bcalculrisque.addEventListener('onclick', valider);

/**/
function recuprecursif(xml){
  var result="";
  var racine = xml.documentElement;
  getFils(racine);
}
/**/
function getFils(xml){
  // document.write( "<b>"+xml.nodeName +"</b> | Type "+ xml.nodeType+" | "+ xml.childNodes.length+ " fils");
  for(var i = 0; i<xml.childNodes.length; i++){
    var element = xml.childNodes[i];

    if(element=="[object CDATASection]"){
      cdata.push(element.nodeValue);
    }
    if(element.nodeType==1){
          getFils(element);
      }
      // else if(element.nodeType==3 || element.nodeType==4){
      //     document.write(" valeur = "+ element.nodeValue);
      // }
  }
}

function checkAlti(){
	var risque = document.getElementById("risque_bas");
	var alti = document.getElementById("seuil_alti");
	var bra_checked = document.getElementById("scales");

	var seuil = document.createElement("input");
	seuil.type = "text";
	seuil.name = "risque_bas";
  seuil.id = "seuil";

	var menuSelect = document.createElement("select");
  menuSelect.id = "menuSelect";
  var option = document.createElement('option');
  option.text = " ";
  menuSelect.add(option);
	for(let i = 1; i < 5; i++){
		var option = document.createElement('option');
		option.text = i;
		menuSelect.add(option);
	}

	if((bra_checked.checked==true)&&(alti.hasChildNodes()==false)){
		alti.appendChild(seuil);
		risque.appendChild(menuSelect);
	}else{
    alti.removeChild(alti.children[0]);
    risque.removeChild(risque.children[0]);
  }
}
