# Yéti - GéoDev²

Projet Développement ING2

# Documentation utilisateur site Web Yéti

# Introduction :

Yéti est un service hébergé sur le site CampToCamp qui aide à la prévention du risque d'avalanche. On le trouve ici : https://www.camptocamp.org/yeti

Ce git se décompose en plusieurs dossiers et sous-dossiers. Le fichier principal est "**index.html**", c'est celui qu'il faut ouvrir en premier. 

Ensuite le dossier "Ressources" contient les sous-dossiers :

    - data : qui contient le fichier "**mountainsWGS84.json**", la source de données des montagnes et de leurs stations qui nous renseigne, entre autres, sur leur bulletin BRA.
    - img : qui contient toutes les images nécessaires au site, notamment les pictogrammes de danger. 
    - script : qui contient le CSS "**yeti.css**" et le javascript "**formulaire.js**".
    

# Documentation utilisateur application mobile Yéti (passage du site sous Cordova)

Cordova a servi à encapsuler le site web que nous avons développé dans une application mobile.  

Sous Cordova il y a donc en plus l'accès à la carte interractive, au menu contenant le formulaire et au plugin Géolocalisation.